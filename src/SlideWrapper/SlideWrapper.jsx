import React from 'react';
import Bounce from 'react-reveal/Bounce';
// import VisibilitySensor from 'react-visibility-sensor';
import './SlideWrapper.css';

export const SlideWrapper = (props) => {

    // const[show, setShow] = useState(false);

    // const onVisibilityChange =(isVisible) => {
    //     setShow(isVisible);
    // }

    // var left = true;
    // var right = false;
    // var top = false;
    // var bottom = false;

    // switch(props.direction) { 
    //     case 'right':
    //         right = true;
    //         left = false;
    //         break;
    //     case 'top':
    //         top = true;
    //         left = false;
    //         break;
    //     case 'bottom':
    //         bottom = true;
    //         left = false;
    //         break;
    //     default:
    // }

    // {/* <VisibilitySensor onChange={onVisibilityChange}>
    //     <Bounce cascade={cascade} >
    //         <div className={classname}>
    //             {props.children}
    //         </div>
    //     </Bounce>
    // </VisibilitySensor> */}

    var cascade = false;
    if (React.Children.count(props.children) > 1){
        cascade = true;
    }

    var classname = 'slide-wrapper';

    if (props.className != null) {
        classname = classname + ' ' + props.className;
    }
    
    return (
        <Bounce cascade={cascade} >
            <div className={classname}>
                {props.children}
            </div>
        </Bounce>
    );
}