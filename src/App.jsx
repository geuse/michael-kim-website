import React, {useState, useEffect, createContext} from 'react';
import './App.css';
import { MainPage } from './Pages/MainPage.jsx';
import * as contentful from 'contentful';

export const ContentfulContext = createContext();

const App = () => {
  const [contentEntries, setContentEntries] = useState();

  const client = contentful.createClient({
    space: process.env.REACT_APP_CONTENTFUL_SPACE, 
    accessToken: process.env.REACT_APP_CONTENTFUL_API_KEY
  });

  useEffect(() => {
    client.getEntries().then(entries => {
      setContentEntries(entries.items);
    });
  }, []);
  
  return (
    <ContentfulContext.Provider value={contentEntries}>
      <MainPage/>
    </ContentfulContext.Provider>
  );
}
export default App;
