import React from 'react';
import './Navbar.css';

export const Navbar = (props) => {
    const navLinks = props.navLinks;
    const navLinksJSX = navLinks.map((linkObj) => 
        <li key={linkObj.text}> <a className="nav-link" href= {linkObj.value} target="_blank">{linkObj.text}</a> </li>
    );

    return (
        <div className='nav-bar'>
            <img className="nav-logo" src={props.navLogo} alt="logo" onClick={()=> window.open("/", "_self")} />
            <ul className="nav-links">
                {navLinksJSX}
            </ul>
        </div>
    );
}