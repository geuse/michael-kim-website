import React from 'react';
import { SyncLoader } from 'react-spinners';
import './LoadingPage.css';

export const LoadingPage = () => {
    return (
      <div className='loading-screen'>
        <SyncLoader
          class='loader'
          color={'#66fcf1'}
          size={20}
          loading={true}/> 
      </div>
    );
}