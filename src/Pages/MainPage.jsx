import React, {useState, useContext} from 'react';
import { ParallaxProvider } from 'react-scroll-parallax';
import { HeroBanner } from '../HeroBanner/HeroBanner';
import { Navbar } from '../Navbar/Navbar';
import { Footer } from '../Footer/Footer';
import { Heading } from '../Heading/Heading';
import { ContentBlock } from '../PageSection/ContentBlock';
import { LoadingPage } from './LoadingPage.jsx';
import { ContentfulContext } from '../App';

export const MainPage = (props) => {
  // React hook for managing state of ref to which the herobanner button should scroll to
  const [headingRef, setHeadingRef] = useState();
  const content = useContext(ContentfulContext);

  // This function is given to the HeroBanner component as a callback to execute the scroll when the banner skip button is clicked
  const scrollOffBanner = () => {
    window.scrollTo({
      top:headingRef.offsetTop,
      behavior: 'smooth'
    });
  };

  // This function is given to the first Heading component as a callback to retrieve its reference
  const setRef = (ref) => {
    setHeadingRef(ref);
  }
  var navContent = null;
  var heroBannerContent = null;
  var contactContent = null;
  var pageContent = null;

  if (!content) {
    return (
      <LoadingPage/>
    );
  }

  content.forEach(entry => {
    if (entry.fields) {
      switch(entry.sys.contentType.sys.id) {
        case 'navBar':
          navContent = entry;
          break;
        case 'heroBanner':
          heroBannerContent = entry;
          break;
        case 'contactSection':
          contactContent = entry;
          break;
        case 'pageContent':
          pageContent = entry;
          break;
        default:

      }
    }
  });

  const pageContentJSX = pageContent.fields.sections.map((section, index) => {
    const headingRefSetter = (index === 0) ? setRef : null;
    return (
      <div key={section.fields.name}>
        <Heading setRef={headingRefSetter} title={section.fields.heading}></Heading>
        {section.fields.contentSections.map(contentBlock => <ContentBlock key={contentBlock.fields.name} content={contentBlock}/>)}
      </div>
    );
  });
  
  return (
    <ParallaxProvider className="App">

      <Navbar navLogo={navContent.fields.logoImage.fields.file.url} navLinks={navContent.fields.navLinks.navLinks} />

      <HeroBanner 
        preHeading={heroBannerContent.fields.preHeading} 
        heading={heroBannerContent.fields.heading} 
        background={heroBannerContent.fields.backgroundImage.fields.file.url} 
        skipButton={heroBannerContent.fields.skipButtonImage.fields.file.url}
        mainImage={heroBannerContent.fields.mainImage.fields.file.url}
        onClick={scrollOffBanner} 
      />

      {pageContentJSX}

      <Footer heading={contactContent.fields.heading} quote={contactContent.fields.quote}/>
    </ParallaxProvider>
  );
}