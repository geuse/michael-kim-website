import React from 'react';
import { PageSection } from '../PageSection/PageSection';
import { SlideWrapper } from '../SlideWrapper/SlideWrapper.jsx';
import { Stats } from '../Stats/Stats.jsx';

export const ContentBlock = (props) => {
  var contentJSX = null;
  switch(props.content.sys.contentType.sys.id) {

    //TODO clean this code up
    case 'contentSection':  
      if (props.content.fields.contentImages.length === 1){
        if (props.content.fields.imageFirst) {
          contentJSX = 
            <PageSection background={props.content.fields.backgroundImage.fields.file.url} backgroundSize='small'>
              <SlideWrapper direction ='left'> <img className="image" src={props.content.fields.contentImages[0].fields.file.url} alt="laptop"/> </SlideWrapper>
              <SlideWrapper direction='right'> <p className="text"> {props.content.fields.contentText.content[0].content[0].value} </p> </SlideWrapper>
            </PageSection> 
        } else {
          contentJSX = 
            <PageSection background={props.content.fields.backgroundImage.fields.file.url} backgroundSize='small'>
              <SlideWrapper direction='left'> <p className="text"> {props.content.fields.contentText.content[0].content[0].value} </p> </SlideWrapper>
              <SlideWrapper direction ='right'> <img className="image" src={props.content.fields.contentImages[0].fields.file.url} alt="laptop"/> </SlideWrapper>
            </PageSection>
        }
      } else {
        if (props.content.fields.imageFirst) {
          contentJSX = 
            <PageSection background={props.content.fields.backgroundImage.fields.file.url} backgroundSize='small'>
              <SlideWrapper direction ='left'>
                <img className="image-under" src={props.content.fields.contentImages[0].fields.file.url} alt="under"/>
                <img className="image-over" src={props.content.fields.contentImages[1].fields.file.url} alt="over"/>
              </SlideWrapper>
              <SlideWrapper direction='right'> <p className="text"> {props.content.fields.contentText.content[0].content[0].value} </p> </SlideWrapper>
            </PageSection>
        } else {
          contentJSX = 
            <PageSection background={props.content.fields.backgroundImage.fields.file.url} backgroundSize='small'>
              <SlideWrapper direction='left'> <p className="text"> {props.content.fields.contentText.content[0].content[0].value} </p> </SlideWrapper>
              <SlideWrapper direction ='right'>
                <img className="image-under" src={props.content.fields.contentImages[0].fields.file.url} alt="under"/>
                <img className="image-over" src={props.content.fields.contentImages[1].fields.file.url} alt="over"/>
              </SlideWrapper>
            </PageSection>
        }
      }
      break;

    case 'locationContentSection':
      var metservice = <iframe title="MetService" id="widget-iframe" width="280px" height="113px" src="https://services.metservice.com/weather-widget/widget?params=white|small|landscape|days-1|classic&loc=auckland&type=urban" style={{border: 'none'}} />
      contentJSX = 
        <PageSection background={props.content.fields.backgroundImage.fields.file.url}>
          <SlideWrapper className='slide-wrapper-location-marker' direction ='top'> <img className="image location-marker-image" src={props.content.fields.locationMarkerImage.fields.file.url} alt="location-marker"/> </SlideWrapper>
          <SlideWrapper className='slide-wrapper-metservice' direction='right'> {metservice} </SlideWrapper>
        </PageSection> 
      break;

    case 'skillsSection':
      console.log(props.content);
      contentJSX = 
        <PageSection background={props.content.fields.backgroundImage.fields.file.url} backgroundSize='small'> 
          <Stats statList={props.content.fields.skills.skills}/>
        </PageSection>
      break;

    default:

  }
  
  return (
    <div>
      {contentJSX}
    </div>
  );
}