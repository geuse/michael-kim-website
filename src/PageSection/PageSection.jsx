import React from 'react';
import './PageSection.css';

export const PageSection = (props) => {
    var bgStyle = null;

    if (props.background != null){
        if (props.backgroundSize === 'small'){
            bgStyle = {
                backgroundImage:`url(${props.background})`,
                backgroundSize:'20vw', 
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
            }
        } else {
            bgStyle = {
                backgroundImage:`url(${props.background})`,
                backgroundSize:'contain', 
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
            }
        }
    }

    return (
        <div className="content-section" style = {bgStyle}>
            {props.children}
        </div>
    );
}