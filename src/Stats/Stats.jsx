import React, {useState}  from 'react';
import { SlideWrapper } from '../SlideWrapper/SlideWrapper.jsx';
import './Stats.css';
import {
    CircularInput,
    CircularTrack,
    CircularProgress,
  } from "react-circular-input";
import VisibilitySensor from 'react-visibility-sensor';
import { Spring } from "react-spring/renderprops.cjs";

export const Stats = (props) => {
    const statList = props.statList;
    const middleIndex = Math.round(statList.length/ 2);
    const leftStats = statList.slice(0, middleIndex );
    const rightStats = statList.slice(middleIndex);

    const leftStatJSX = leftStats.map((statObj) => 
        <div key={statObj.text}><Stat text={statObj.text} skillLevel = {statObj.value}/></div>
    );

    const rightStatJSX = rightStats.map((statObj) => 
        <div key={statObj.text} ><Stat text={statObj.text} skillLevel = {statObj.value}/></div>
    );

    return (
        <div className='all-stats'>
            <SlideWrapper className="left-stats" direction ='bottom'>
                {leftStatJSX}
            </SlideWrapper>
            <SlideWrapper className="right-stats" direction ='bottom'>
                {rightStatJSX}
            </SlideWrapper>
        </div>
    );
}

const Stat = (props) => {
    const[value, setValue] = useState(0);
    
    var radius = 30;
    if (props.radius != null) {
        radius = props.radius;
    };
    
    const onVisibilityChange = (isVisible) => {
        setValue((isVisible) ? props.skillLevel: 0);
    }

    return (
            <div className='stat-component'>
                <Spring from={{value:0}} to={{value}} config={{tension: 280, friction: 200, delay: 300}}>
                    {props => (
                        <VisibilitySensor onChange={onVisibilityChange}>
                            <CircularInput radius={radius} value={props.value}>
                                <CircularTrack className='circular-track'/>
                                <CircularProgress className='circular-progress'/>
                            </CircularInput>
                        </VisibilitySensor>
                    )}
                </Spring>
                <h3 className='stat-text'>{props.text}</h3>
            </div>
    );
}