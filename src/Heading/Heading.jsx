import React from 'react';
import './Heading.css';

export const Heading = (props) => {
    return (
        <div ref={props.setRef} className="heading"><strong className="heading-title"> {props.title}</strong></div>
    );
}