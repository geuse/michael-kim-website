import React from 'react';
import { ParallaxBanner } from 'react-scroll-parallax';
import './HeroBanner.css';

export const HeroBanner = (props) => {
    const buttonStyle = {
        backgroundImage:`url(${props.skipButton})`,
    }

    return (
        <ParallaxBanner
            className="hero-banner"
            layers={[
                {
                    image: props.background,
                    amount: 0.25,
                },
            ]}
            style={{
                height: '100vh',
                width: '100%',
            }}
        >
            <div className='banner-content'>
                <em className="banner-intro">{props.preHeading}</em>
                <strong className="banner-title">{props.heading}</strong>
                <img className="banner-avatar" src={props.mainImage} alt="avatar"/>
                <button style={buttonStyle} className="banner-skip" onClick={props.onClick}>  </button>
                {/* <img class="button-image" src={require('../assets/button-down.png')} alt="button"/> */}
            </div>
        </ParallaxBanner>
    );
}