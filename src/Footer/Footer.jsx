import React from 'react';
import './Footer.css';

export const Footer = (props) => {
    return (
        <div className="footer">
            <span className="contact-title">{props.heading}</span>
            <em>{props.quote}</em>
        </div>
    );
}
